# Heroku Portfolio

## Tentang Project ini

Merupakan boilerplate untuk memulai portofolio yang bisa dideploy ke heroku

### Instalasi

#### Linux

Diharapkan sudah mempunyai :

+ GIT
+ [Composer](<https://getcomposer.org/>)
+ [Akun Heroku](<https://heroku.com>)

##### Lokal

1. Clone atau download repository ini
2. Copykan ke /var/www/html atau ke public_html kalian masing - masing
3. Disini saya menggunakan virtual host, jadi settings virtual host kalian masing - masing, atau bisa lihat tutorial [disini](<https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-virtual-hosts-on-ubuntu-16-04>)
4. Setting postgresql
    + Untuk langkah lebih lengkap bisa kunjungi [Wiki](<https://wiki.postgresql.org/wiki/Apt>) atau [digital ocean tutorial](<https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-18-04>)
5. Copykan ```.env.example``` menjadi ```.env```
6. Isikan ```.env``` tadi sesuai dengan postgre sql kalian
7. Lakukan ```COmposer Install``` untuk menginstall dependencies

##### Heroku

1. Install heroku CLI di PC/Laptop kalian, untuk settings heroku cli cek tutorial [disini](<https://devcenter.heroku.com/articles/heroku-cli#standalone-installation>) atau khusus untuk PHP Tutorial ada [disini](<https://devcenter.heroku.com/articles/getting-started-with-php>)
2. Push ke heroku kalian, kemudian pada dashboard akun heroku kalian maka akan muncul app yang telah kalian push
3. Pada tabs ```Resources``` ada add-ons, tambahkan Heroku Postgres
4. Arahkan ke folder yang telah kalian buat, pada CLI kalian ```heroku config```, copykan DATABASE_URL ke ```.env``` kalian
5. Push lagi ke heroku kalian

#### Windows

Belum dicoba
