<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LandingPageController extends CI_Controller {

	public function index(){
		redirect(route('website.id.index'));
	}
}
