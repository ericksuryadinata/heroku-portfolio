<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WebsiteController extends CI_Controller {

	public function index(){
		echo $this->page->tampil('website.en.index');
	}

	public function aboutMe(){
		echo $this->page->tampil('website.en.about-me');
	}
	
	public function skills(){
		echo $this->page->tampil('website.en.skills');
	}

	public function portfolio(){
		echo $this->page->tampil('website.id.portfolio');
	}

	public function experience(){
		echo $this->page->tampil('website.id.experience');
	}

	public function contactMe(){
		echo $this->page->tampil('website.id.contact-me');
	}
}
