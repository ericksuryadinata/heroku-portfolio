<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WebsiteController extends CI_Controller {

	public function index(){
		echo $this->page->tampil('website.id.index');
	}

	public function tentangSaya(){
		echo $this->page->tampil('website.id.partials.tentang-saya');
	}
	
	public function keahlian(){
		echo $this->page->tampil('website.id.partials.keahlian');
	}

	public function portfolio(){
		echo $this->page->tampil('website.id.partials.portfolio');
	}

	public function pengalamanKerja(){
		echo $this->page->tampil('website.id.partials.pengalaman-kerja');
	}

	public function hubungiSaya(){
		echo $this->page->tampil('website.id.partials.hubungi-saya');
	}
}
