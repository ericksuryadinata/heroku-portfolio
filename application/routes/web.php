<?php

/**
 * Welcome to Luthier-CI!
 *
 * This is your main route file. Put all your HTTP-Based routes here using the static
 * Route class methods
 *
 * Examples:
 *
 *    Route::get('foo', 'bar@baz');
 *      -> $route['foo']['GET'] = 'bar/baz';
 *
 *    Route::post('bar', 'baz@fobie', [ 'namespace' => 'cats' ]);
 *      -> $route['bar']['POST'] = 'cats/baz/foobie';
 *
 *    Route::get('blog/{slug}', 'blog@post');
 *      -> $route['blog/(:any)'] = 'blog/post/$1'
 */

 /**
  * For educational purposes only
  */
Route::group('/', function(){
    Route::get('/','LandingPageController@index');
    Route::group('id',['namespace'=>'Indonesia'], function () {
        Route::get('/','WebsiteController@index')->name('website.id.index'); 
        Route::get('/tentang-saya','WebsiteController@tentangSaya')->name('website.id.tentang-saya');
        Route::get('/keahlian','WebsiteController@keahlian')->name('website.id.keahlian');
        Route::get('/portfolio','WebsiteController@portfolio')->name('website.id.portfolio');
        Route::get('/pengalaman-kerja','WebsiteController@pengalamanKerja')->name('website.id.pengalaman-kerja');
        Route::get('/hubungi-saya','WebsiteController@hubungiSaya')->name('website.id.hubungi-saya');
    });

    Route::group('en',['namespace'=>'English'], function () {
        Route::get('/','WebsiteController@index')->name('website.en.index'); 
        Route::get('/about-me','WebsiteController@aboutMe')->name('website.en.about-me');
        Route::get('/skills','WebsiteController@skills')->name('website.en.skills');
        Route::get('/portfolio','WebsiteController@portfolio')->name('website.en.portfolio');
        Route::get('/experience','WebsiteController@experience')->name('website.en.experience');
        Route::get('/contact-me','WebsiteController@contactMe')->name('website.en.contact-me'); 
    });
});

Route::set('404_override', function(){
    show_404();
});

Route::set('translate_uri_dashes',FALSE);