<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<meta name="author" content="Erick Surya Dinata">
<meta name="keyword" content="Erick Surya, proesd, Erick Surya Dinata">
<title>@yield('title')</title>