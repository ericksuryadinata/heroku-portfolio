
<section class="section is-header">
    <div class="container">
        <nav class="navbar">
            <div class="navbar-brand">
                <a href="{{base_url()}}" class="navbar-item">
                    <p>ESD</p>
                </a>
                <a role="button" class="navbar-burger" data-target="navMenu" aria-label="menu" aria-expanded="false">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>
            <div class="navbar-menu" id="navMenu">
                <div class="navbar-end">
                    <span class="navbar-item">
                        <a href="{{route('website.id.tentang-saya')}}" class="is-link navbar-item">Tentang Saya</a>
                    </span>
                    <span class="navbar-item">
                        <a href="{{route('website.id.keahlian')}}" class="is-link navbar-item">Keahlian</a>
                    </span>
                    <span class="navbar-item">
                        <a href="{{route('website.id.portfolio')}}" class="is-link navbar-item">Portfolio</a>
                    </span>
                    <span class="navbar-item">
                        <a href="{{route('website.id.pengalaman-kerja')}}" class="is-link navbar-item">Pengalaman Kerja</a>
                    </span>
                    <span class="navbar-item">
                        <a href="{{route('website.id.hubungi-saya')}}" class="is-link navbar-item">Hubungi Saya</a>
                    </span>
                </div>
            </div>
        </nav>
    </div>
</section>