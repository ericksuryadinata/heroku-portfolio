@extends('website.layout')

@section('title','Erick Surya Dinata')

@section('content')
    @include('website.id.includes.navbar')
    @include('website.id.partials.home')
    @include('website.id.partials.tentang-saya')
    @include('website.id.partials.keahlian')
    @include('website.id.partials.portfolio')
    @include('website.id.partials.pengalaman-kerja')
    @include('website.id.partials.hubungi-saya')
    @include('website.id.includes.footer')
@stop